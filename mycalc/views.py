from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
def suma(request, num1, num2):
    suma = num1 + num2
    return HttpResponse(f'La suma de {num1} y {num2} es {suma}')

def resta(request, num1, num2):
    resta = num1 - num2
    return HttpResponse(f'La resta de {num1} menos {num2} es {resta}')
def multi(request, num1, num2):
    multi = num1 * num2
    return HttpResponse(f'La multiplicación de {num1} y {num2} es {multi}')

def div(request, num1, num2):
    div = num1 / num2
    return HttpResponse(f'La división de {num1} entre {num2} es {div}')


def menu(request):
    return HttpResponse('Bienvenido al menú. Elige suma, resta, división o multiplicación')